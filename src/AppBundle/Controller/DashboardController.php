<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/", name="show_dashboard")
     */
    public function showAction(Request $request)
    {
        return $this->render('AppBundle:Dashboard:show.html.twig');
    }
}
